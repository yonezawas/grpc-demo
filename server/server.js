var grpc = require("grpc");
var todo = grpc.load("./protos/todos.proto");
var todos = [
  { id: "1", title: "Note 1", content: "Content 1" },
  { id: "2", title: "Note 2", content: "Content 2" }
];

function main() {
  var server = new grpc.Server();
  server.addService(todo.TodoService.service, {
    list: (_, callback) => {
      callback(null, todos);
    },
    insert: (call, callback) => {
      var todo = { ...call.request };
      todo.id = (1 + todos.length).toString();
      todos.push(todo);
      callback(null, {});
    },
    delete: (call, callback) => {
      console.log(call);
      todos = todos.filter(todo => todo.id !== call.request.id);
      callback(null, {});
    }
  });
  server.bind("0.0.0.0:50051", grpc.ServerCredentials.createInsecure());
  server.start();
}

main();
