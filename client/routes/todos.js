var express = require("express");
var client = require("../client");
var router = express.Router();

router.post("/", function(req, res, next) {
  var { title, content } = req.body;
  client.insert({ title, content }, (err, {}) => {
    if (err) next(err);
    res.redirect("/");
  });
});

router.get("/:id", function(req, res, next) {
  var { id } = req.params;
  client.delete({ id }, (err, {}) => {
    if (err) next(err);
    res.redirect("/");
  });
});

module.exports = router;
