var express = require("express");
var client = require("../client");
var router = express.Router();

router.get("/", function(req, res, next) {
  client.list({}, (err, { todos }) => {
    if (err) next(err);
    res.render("index", { title: "Express", todos });
  });
});

module.exports = router;
