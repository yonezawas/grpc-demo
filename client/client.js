var grpc = require("grpc");
var { TodoService } = grpc.load("./protos/todos.proto");

module.exports = new TodoService(
  "server:50051",
  grpc.credentials.createInsecure()
);
